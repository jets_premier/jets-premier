import React from 'react';
import { Link } from 'react-router-dom';
import { GiHamburgerMenu } from 'react-icons/gi';
import { CiCircleRemove } from 'react-icons/ci';

import images from '../../constants/images';

import './Navbar.css';

const Navbar = () => {
    const [ toggleMenu, setToggleMenu ] = React.useState(false);

    return (
        <nav className='app__navbar'>
            <div className='app__navbar-logo'>
                <Link to="/">
                <img src={images.logo} alt="logo" />
                </Link>
            </div>
            <ul className='app__navbar-links'>
                <li><a href="#home">Inicio</a></li>
                <li><a href="#about">¿Quiénes somos?</a></li>
                <li><a href="#servicios">Servicios</a></li>
                <li><a href="#contacto">Contacto</a></li>
            </ul>

            <div className='app__navbar-smallscreen'>
                <GiHamburgerMenu fontSize={27}  onClick={() => setToggleMenu(true)}/>
                
                {toggleMenu && (
                <div className='app__navbar-smallscreen_overlay flex__center slide-bottom'>
                    <CiCircleRemove font-size={27} className="overlay__close" onClick={() => setToggleMenu(false)}/>
                    <ul className='app__navbar-smallscreen-links'>
                        <li><a href='#home'>Inicio</a></li>
                        <li><a href='#about'>¿Quiénes somos?</a></li>
                        <li><a href='#servicios'>Servicios</a></li>
                        <li><a href='#contacto'>Contacto</a></li>
                        <li><a href='#ingresar'>Ingresar</a></li>
                        <li><a href='#registrarse'>Registrarse</a></li>
                    </ul>
                </div>
                )}
            </div>
        </nav>
    )
}

export default Navbar;
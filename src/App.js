import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Home } from './container';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Jets from './pages/jets/Jets';

import './App.css';

const App = () => (
  <div>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="login" element={<Login />}></Route>
        <Route path="register" element={<Register />}></Route>
        <Route path="jets" element={<Jets />}></Route>
      </Routes>
    </BrowserRouter>
  </div>
);

export default App;


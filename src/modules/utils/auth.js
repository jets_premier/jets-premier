import axios from "axios";
const urlAuth = process.env.REACT_APP_HOST + "/auth";

export const SignUp = (body) => {
    const urlRegister = urlAuth + "/register";
    return axios.post(urlRegister, body);
  };
import React from 'react';
import { FiFacebook, FiTwitter, FiInstagram} from 'react-icons/fi';

// import { FooterOverlay } from '../../components';
import images  from '../../constants/images';

import './Footer.css'

const Footer = () => (
    <div className='app__footer section__padding'>

        <div className='app__footer-links' id='contacto'>
            <div className='app__footer-links_contact'>
                <h1 className='app__footer-headtext'>Contactanos</h1>
                <h4>Teléfono: </h4>
                <p>+52 668 111 0331</p>
                <h4>Correo electrónico: </h4>
                <p>jgosuna@netz.com.mx</p>
            </div>
            <div className='app__footer-links_logo'>
                <img src={images.logo_black} alt="logo" />
                <p>Descubre la diferencia de Jets Premier</p>
                <div className='app__footer-links_icons'>
                    <FiFacebook className='app__iconFacebook'/>
                    <FiTwitter className='app__iconTwitter' />
                    <FiInstagram className='app__iconInstagram' />
                </div>
            </div>
            <div className='app__footer-links_work'>
                <h1 className='app__footer-headtext'>Horarios de atención</h1>
                <p>Las 24 horas del día incluyendo los dias festivos</p>
                <p>Domicilio: </p>
                <p>Vialidad CH-P #4612</p>
                <p>Código Postal: </p>
                <p>31030</p>
                <p>Colonia: </p>
                <p>Rosario, Chihuahua Chihuahua</p>
            </div>
        </div>
        <div className='footer__copyrigth'>
            <p>2023 Jets Premier, All Rights reserved.</p>
        </div>
    </div>
);

export default Footer;
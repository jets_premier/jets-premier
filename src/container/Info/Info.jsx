import React from 'react';

import {IoDiamondOutline} from 'react-icons/io5';
import {MdOutlineSecurity} from 'react-icons/md';
import {SiFastly} from 'react-icons/si';

import {FaCcVisa} from 'react-icons/fa';
import {FaCcMastercard} from 'react-icons/fa'
import {SiAmericanexpress} from 'react-icons/si';
import {BsPaypal} from 'react-icons/bs';

import './Info.css';

const Info = () => {
    return(
        <div className='app__info-content' id="about">
            <h1>Somos JETS PREMIER</h1>
            <p>Jet Premier es una aerolínea regional, de capital privado, con localización en Chihuahua. Contamos con más de 4 años en función ininterrumpidamente, actualmente cuenta con una flota operativa de dos jet y cuatro altamente capacitados pilotos.</p>
            <p>Jet Premier actualmente cuenta con una variedad de rutas en distintos destinos nacionales todos saliendo de Chihuahua cómo origen.</p>
            <h2>Características del vuelo</h2>
            <div className='app__info-icons'>
                <IoDiamondOutline /><br />
                <SiFastly /><br />
                <MdOutlineSecurity />
            </div>
            <div className='app__info_pay'>
                <h3>Se cuenta con los siguientes métodos de pago: </h3>
                <FaCcVisa />
                <FaCcMastercard />
                <SiAmericanexpress />
                <BsPaypal />
            </div>
        </div>
    );
}

export default Info;
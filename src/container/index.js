import Input from './Input/Input';
import Home from './Home/Home';
import Footer from './Footer/Footer';
import Services from './Services/Services';
import Info from './Info/Info';

export {
    Input,
    Home,
    Footer, 
    Services,
    Info
}
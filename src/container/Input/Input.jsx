import React from 'react';
import { Link } from 'react-router-dom';
// React Icons
import {SlPlane} from 'react-icons/sl';

import './Input.css';

const Input = () => (
    <div className='app__inputCta'>
        <div className='app__inputContent'>
            <SlPlane fontSize={30} />
            <h2>Descubre la diferencia de Jets Premier</h2>
            <div className='app_inputBtn'>
                <Link to="https://jetspremier-one.vercel.app/">
                <button type='button'>Ingresar</button>
                </Link>
                <Link to="https://jetspremier-one.vercel.app/">
                <button type='button'>Registrarse</button>
                </Link>
            </div>
        </div>
    </div>
);

export default Input;
import React from 'react';
import { Link } from 'react-router-dom';
import { BsArrowLeftShort, BsArrowRightShort } from 'react-icons/bs';

import images from '../../constants/images';

import './Services.css';

const servicesImages = [images.airplane_1, images.airplane_2, images.airplane_3, images.airplane_4];

const Services = () => {

    const scrollRef = React.useRef(null);

    // Funcion del scroll
    const scroll = (direction) => {
        const { current } = scrollRef;

        if (direction === 'left') {
            current.scrollLeft -= 300;
        } else {
            current.scrollLeft += 300;
        }
    }

    return (
        <div id="servicios" className='app__services flex__center'>
            <div className='app__services-content'>
                <h1>Nuestros Servicios</h1>
                <h2>Renta de Jets Privados</h2>
                <p>Contamos con los mejores vuelos para cubrir cualquier necesidad para cubrir cualquier necesidad Empresarial, Personal, Familiar con vuelos de cualquier ubicación deseada.</p>
                <h2>Bienestar</h2>
                <p>En Jets Premier nos preocupamos por tu comodidad, en este apartado encontrarás una galería donde se mostrarán imágenes del equipo que garantizan un viaje 100% Satisfactorio.</p>
                <Link to="https://jetspremier-one.vercel.app/">
                    <button type="button" className='custom__button'>RESERVA AHORA</button>
                </Link>
            </div>

            <div className='app__services-images'>
                <div className='app__services-images_container' ref={scrollRef}>
                    {servicesImages.map((image, index) => (
                        <div className='app__services-images_card flex__center' key={`services_image-${index + 1}`}>
                            <img src={image} alt="services_img" />
                        </div>
                    ))}
                </div>
                <div className='app__services-images_arrow'>
                    <BsArrowLeftShort className='services__arrow-icons' onClick={() => scroll('left')} />
                    <BsArrowRightShort className='services__arrow-icons' onClick={() => scroll('right')} />
                </div>
            </div>
        </div>
    );
}

export default Services;
import React from 'react';
import { Link } from 'react-router-dom';
import { AiFillCaretRight } from 'react-icons/ai';

import Navbar from '../../components/Navbar/Navbar'
import Input from '../Input/Input';
import Services from '../Services/Services';

import images from '../../constants/images';

import Info from '../Info/Info';
import Footer from '../Footer/Footer';

import './Home.css';


const Home = () => (
    <>
        <Navbar />
        <Input />

        <div className='app__home-section' id="home">
            <img src={images.Home} alt="home-img" />
            <h1>PRIVATE FLIGHTS</h1>
            <h3>Reserva tus próximos vuelos</h3>
            <Link                 to="https://jetspremier-one.vercel.app/">
                <button
                type="button"
                >
                Ver más<AiFillCaretRight className='app__home-icon' />
                </button>
            </Link>

        </div>
        <Services />
        <Info />
        <Footer />
    </>
);

export default Home;
import React from 'react'

import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

import Jet from './Jet'
import Jet_data from '../../constants/jets-data'


import Navbar from '../../components/Navbar/Navbar';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const Jets = () => {
  return (
    <div>
      <Navbar />
      <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        {
          Jet_data.map(jet =>(
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <Jet key={jet.id} jet={jet} />
            </Grid>
          ))
        }

      </Grid>
    </Box>
    </div>
  )
}

export default Jets
import React from 'react'

import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AirplaneTicketIcon from '@mui/icons-material/AirplaneTicket';
import { createTheme } from '@mui/system';

import accounting from "accounting";

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

const Jet = ({ jet: { id, name, price, image, description, is_enable } }) => {
    const theme = createTheme();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    }

    return (
        <Card sx={{margin: '15px'}}>
            <CardHeader
                action={
                    <Typography
                        aria-label="settings"
                        variant="h5"
                        color="textSecondary">
                        <p>Día: {accounting.formatMoney(price)}</p>
                    </Typography>
                }
                title={name}
                subheader={is_enable}
            />
            <CardMedia
                component="img"
                height="auto"
                image={image}
                alt={name}
            />
            <CardActions disableSpacing>
                <IconButton aria-label="Rentar">
                    <AirplaneTicketIcon fontSize="large" />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography paragraph>{description}</Typography>
                </CardContent>
            </Collapse>
        </Card>
    )
}

export default Jet
import React from 'react'
import { HiOutlineMail } from 'react-icons/hi'
import { RiLockPasswordFill } from 'react-icons/ri'

import LogImg from '../../assets/JetsLogin.jpeg'

import './login.css'

const Login = () => {
    return (
        <div className='app__login'>
            <div className='app__login-container'>
                <input type="checbox" id="flip" hidden />
                <div className="app__login-cover">
                    <div className="app__login-front">
                        <img className='app__login-img' src={LogImg} alt="jet_login" />
                        <div className="app__login-text">
                            <span className="text-1">Jets Premier <br /> Disfruta Tu Privacidad </span>
                            <span className="text-2">Sin Limites</span>
                        </div>
                    </div>
                </div>
                <div className="app__login-forms">
                    <div className="form-content">
                        <div className="login-form">
                            <div className="title">Login</div>
                            <form action="#">
                                <div className="input-boxes">
                                    <div className="input-box">
                                        <HiOutlineMail />
                                        <input type="text" placeholder='Introduzca Su Email' required />
                                    </div>
                                </div>
                                <div className="input-box">
                                    <RiLockPasswordFill />
                                    <input type="password" placeholder='Introduzca Su Contraseña' required />
                                </div>
                                <div className="text"><a href="#">¿Olvidaste tu contraseña?</a></div>
                                <div className="button input-box">
                                    <input type="submit" value="Entrar"/>
                                </div>
                                <div className='text'><a href="#">Ya tengo una cuenta</a></div>
                                <div className='app__login-volver text'><a href="/">Volver</a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
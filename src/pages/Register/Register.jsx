import React from 'react'
// import { Link } from "react-router-dom";

import Swal from "sweetalert2";

import { SignUp } from "../../modules/utils/auth";

import './register.css'

const Register = () => {
    const submitHandler = (e) => {
        e.preventDefault();
        const body = {
            name: e.target.name.value,
            phone: e.target.phone.value,
            email: e.target.email.value,
            password: e.target.password.value,
            gender: e.target.gender.value
        };
        SignUp(body)
            .then((res) => {
                Swal.fire({
                    icon: "success",
                    title: "Register Success",
                    text: `${res.data.result.msg}`,
                });
            })
            .catch(({ ...err }) => {
                Swal.fire({
                    icon: "error",
                    title: "There is an error ?",
                    text: `${err.response.data.err}`,
                });
            });
    };


    return (
        <div className='app__register'>
            <div className='app__register-container'>
                <div className="title">Registrate</div>
                <div className="app__register-content">
                    <form onSubmit={submitHandler}>
                        <div className="user-details">
                            <div className="input-box">
                                <span className='details'>Nombre Completo</span>
                                <input 
                                type="text" 
                                name="name"
                                id="name"
                                placeholder='Introduzca su nombre' 
                                required />
                            </div>
                            <div className='input-box'>
                                <span className="details">Email</span>
                                <input 
                                type="text"
                                name="email"
                                id="email"
                                placeholder="Introduzca Su Email" 
                                required></input>
                            </div>
                            <div className="input-box">
                                <span className="details">Numero Telefonico</span>
                                <input 
                                type="number"
                                name="phone"
                                id="phone" 
                                placeholder="Introduzca Su Numero" 
                                required />
                            </div>
                            <div className="input-box">
                                <span className="details">Contraseña</span>
                                <input 
                                type="password" 
                                name="password"
                                id="password"
                                placeholder="Introduzca Su Contraseña" 
                                required />
                            </div>
                        </div>
                        <div className="gender-details">
                            <input type="radio" name="gender" id="dot-1" value="Masculino"/>
                            <input type="radio" name="gender" id="dot-2" value="Femenino"/>
                            <input type="radio" name="gender" id="dot-3" value="Prefiero_no_decir"/>
                            <span className="gender-title">Genero</span>
                            <div className="category">
                                <label for="dot-1">
                                    <span className="dot one"></span>
                                    <span className="gender">Masculino</span>
                                </label>
                                <label for="dot-2">
                                    <span className="dot two"></span>
                                    <span className="gender">Femenino</span>
                                </label>
                                <label for="dot-3">
                                    <span className="dot three"></span>
                                    <span className="gender">Prefiero no decir</span>
                                </label>
                            </div>
                        </div>
                        <div className="button">
                            <input type="submit" value="Registrarse" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Register
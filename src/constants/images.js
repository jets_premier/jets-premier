import logo from '../assets/logo.jpeg';
import Home from '../assets/Home.jpeg';
import logo_black from '../assets/logo_black.png';
import airplane_1 from '../assets/Airplane_1.jpeg'
import airplane_2 from '../assets/Airplane_2.jpeg'
import airplane_3 from '../assets/Airplane_3.jpeg'
import airplane_4 from '../assets/Airplane_4.jpeg'


export default {
    logo,
    Home,
    logo_black,
    airplane_1,
    airplane_2,
    airplane_3,
    airplane_4
}
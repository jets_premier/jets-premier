const Jets = [
    {
        id: 1,
        name: "Jet",
        price: 10000,
        image: 'assets/Jet-2.jpeg',
        description: 'Comodo Jet Privado moderno',
        is_enable: 'Disponible'
    },
    {
        id: 2,
        name: "Jet",
        price: 10000,
        image: 'assets/Jet.jpeg',
        description: 'Comodo Jet Privado moderno',
        is_enable: 'Disponible'
    }
]

export default Jets